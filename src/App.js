import logo from './logo.svg';
import './App.css';
import React from 'react';
import Home from "./views/home/home";
import Main from './components/Main.component';

export default class App extends React.Component {

    render(){
    return (
        <div className="App">
            <Main/>
        </div>
    );
 }
}
