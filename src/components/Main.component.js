import React from 'react'
import Day from './horloge/Day.component'
import Hour from './horloge/Hour.component'
import DateComp from './horloge/Date.component'
import Period from './horloge/Period.component'
import Update from './Update.component'

export default class Main extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            time: new Date(),
            isUpdate: false,
            reset: false
        }
    }

    componentDidMount() {
        this.currentTime = setInterval(() => {
          const { time } = this.state
          time.setTime(time.getTime() + 1000);
          this.setState({ time: new Date(time.getTime()) });
        }, 1000);
      }
    
      componentWillUnmount() {
        clearInterval(this.currentTime);
      }

     _handleChange = (date) => {
         date =  date === null ? new Date() : date
         this.setState({time: date})
    } 

    _handleReset = () => {
        this.setState({time : new Date() , reset: !this.state.reset})
    }

    render() {
        const { time , isUpdate , reset } = this.state
        return(
            <div className="container">
                {
                    isUpdate ?
                    <Update selected={time}  onChange={this._handleChange} Back={() => this.setState({isUpdate: false})} /> :
                    <div>
                        <Day initDay={time.toLocaleDateString([] , { weekday: 'long' })} reset={reset}/>
                        <Period time={this.state.time}/>
                        <Hour initHour={time.toLocaleTimeString([] , { hour: 'numeric' , minute: 'numeric' })} reset={reset}/>
                        <DateComp initDate={time.toLocaleDateString([] , { year: 'numeric' , month: 'long' , day: 'numeric' })} reset={reset}/>
                        <button onClick={() => this.setState({isUpdate: true})}>Edit</button>
                        <button onClick={this._handleReset}>Reset</button>
                    </div>
                    
                } 
            </div>
        )
    }
}