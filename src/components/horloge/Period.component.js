import React from 'react'

const Period = ({ time }) => {
    let message = ""

    if(time.getHours() < 13 ) {
        message = "Matinée"
    }else if ( time.getHours() < 18) {
        message = "Après midi"
    } else {
        message = "Soirée"
    }

    return (
        <div>{message}</div>
    )
}

export default Period