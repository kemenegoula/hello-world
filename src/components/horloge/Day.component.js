import React , { useState , useEffect } from 'react'

const Day = ({ initDay , reset }) => {

    const [ day , setDay ] =  useState(null)

    useEffect(() => {
        setDay(initDay)
    }, [initDay , reset])
    
    return (
        <div>{day?.toUpperCase()}</div>
    )
}

export default Day