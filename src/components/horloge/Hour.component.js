import React , { useState , useEffect } from 'react'

const Hour = ({ initHour , reset }) => {

    const [ hour , setHour ] =  useState(null)

    useEffect(() => {
        setHour(initHour)
    }, [initHour , reset])
    
    return (
        <div>{hour?.toUpperCase()}</div>
    )
}

export default Hour