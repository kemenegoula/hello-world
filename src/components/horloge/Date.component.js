import React , { useState , useEffect } from 'react'

const Date = ({ initDate , reset }) => {

    const [ date , setDate ] =  useState(null)

    useEffect(() => {
        setDate(initDate)
    }, [initDate , reset])
    
    return (
        <div>{date?.toUpperCase()}</div>
    )
}

export default Date