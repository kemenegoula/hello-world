import React from "react";
import {makeStyles} from "@material-ui/core";

export default function SendBoxButton ({value, hello}){
    const classes = useStyle();

    return <div>
        <input type="text"/>
        <button className={classes.root} onClick={hello}>{value}</button>
    </div>
}

const useStyle = makeStyles((theme)=>(
    {
        root:{
            backgroundColor:'#f00000'
        }
    })
)
