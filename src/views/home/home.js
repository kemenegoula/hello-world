import React from "react";
import SendBoxButton from "../../components/sendBoxButton";

export default  class Home extends React.Component{

    hello(e){
        console.log(e)
        alert('bonjour '+e)
    }

    render(){
        return <div>
            <div>
                <SendBoxButton value={'bonjour'} hello={()=>this.hello('fffff')}/>
                <SendBoxButton value={'bonsoir'}/>
                <SendBoxButton/>
            </div>
            <div>Accusamus at atque enim excepturi facere iste iure modi quo? Eaque, magnam, quaerat. Architecto
                deserunt ea perferendis quam quia quidem rerum sapiente soluta voluptas voluptate! Adipisci
                exercitationem non quo soluta?
            </div>
            <div>Accusamus aperiam blanditiis cum, delectus distinctio est exercitationem ipsum nihil placeat provident
                quaerat quibusdam quis, repellendus sit temporibus velit voluptas voluptatum. Earum laborum molestiae
                recusandae sed vero. Nobis, suscipit, unde!
            </div>
            <div>Delectus dolore incidunt libero, nulla odio quam suscipit tempore? Amet beatae cumque deserunt
                dignissimos doloremque eius ex, id iste, nemo nihil quae quidem quo reiciendis sint soluta totam vero
                vitae?
            </div>
            <div>Architecto beatae blanditiis ea eligendi est excepturi illum impedit inventore ipsum iste libero
                maiores minus molestias numquam odio officiis, provident quam quibusdam quisquam quo reprehenderit
                repudiandae rerum sapiente, sunt vitae?
            </div>
        </div>
    }
}
